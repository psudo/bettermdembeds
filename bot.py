import os
import discord
from dotenv import load_dotenv
import requests

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if 'mangadex.org' in message.content:
        urls = [
            s for _, s in enumerate(
                message.content.split()) if 'mangadex.org' in s]
        remove_suppressed = [i for i in urls if '<' not in i or '>' not in i]
        remove_users = [i for i in remove_suppressed if 'user' not in i]
        split = [i.split('/') for i in remove_users]
        stripped = [i[i.index('mangadex.org') + 1:] for i in split]
        for i in stripped:
            result = process_data(message.author, *i)
            await message.channel.send(embed=result)


def process_data(message_author, endpoint, slug, page=None):
    chapter_info = ''
    if endpoint == 'chapter':
        chapter = requests.get(f'https://api.mangadex.org/chapter/{slug}')
        if chapter.status_code == 200:
            data = chapter.json().get('data', {})
            attrs = data.get('attributes', {})
            chapter_number = attrs.get('chapter', '').replace('.', '-')
            page_num = page if(page) else 1
            chapter_info = f'{chapter_number}/{page_num}'
            rels = data.get('relationships', {})
            for r in rels:
                if r.get('type', '') == 'manga':
                    chapter_slug = slug
                    slug = r.get('id', slug)
        else:
            return embed(
                error=f'Unable to find manga asscoiated with chapter id: {slug} sent by {message_author}.')

    manga_request = requests.get(
        'https://api.mangadex.org/manga/{}?includes[]=author&includes[]=artis&includes[]=cover_art'.format(slug))

    if manga_request.status_code == 200:
        data = manga_request.json().get('data')
        # Turns out MD can't honor it's own spec, and might return any empty structure instead of the object they
        # claim, so coerce it into what I want
        data = data if data else {}
        attrs = data.get('attributes')
        attrs = attrs if attrs else {}
        relationships = data.get('relationships')
        relationships = relationships if relationships else {}
        # Because the language of the title is inconsistent, just grab the
        # first one it finds
        title = list(attrs.get('title', {'any': None}).values())[0]
        desc = attrs.get('description')
        desc = desc if desc else {}
        en_desc = desc.get('en', None)
        author = None
        artist = None
        cover = None
        for rel in relationships:
            attr = rel.get('attributes')
            attr = attr if attr else {}
            if rel.get('type', '') == 'author':
                author = attr.get('name', None)
                if author is None:
                    author_id = rel.get('id', '')
                    author = expand_author(author_id)
            if rel.get('type', '') == 'artist':
                artist = attr.get('name', None)
                if artist is None:
                    artist_id = rel.get('id', '')
                    artist = expand_author(artist_id)
            if rel.get('type', '') == 'cover_art':
                cover = attr.get('fileName', None)
        return embed(
            title,
            en_desc,
            author,
            artist,
            cover,
            slug,
            chapter_info,
            message_author)
    else:
        return embed(
            error=f'Unable to load manga with id: {slug} sent by {message_author}.')


def expand_author(author_id):
    # Sometimes authors/artists don't auto expand
    author_req = requests.get(f'https://api.mangadex.org/author/{author_id}')
    if author_req.status_code == 200:
        data = author_req.json().get('data')
        data = data if data else {}
        attr = data.get('attributes')
        attr = attr if attr else {}
        name = attr.get('name')
        name = name if name else None
        return name


def embed(
        title=None,
        desc=None,
        author=None,
        artist=None,
        cover=None,
        slug=None,
        chapter_info='',
        message_author='',
        error=None):
    if error:
        return discord.Embed(
            title=f'Unable to load information from the link',
            description=error,
            color=discord.Color.red())

    manga_embed = discord.Embed(
        title=title,
        description=shorten_desc(desc),
        url=f'https://cubari.moe/read/mangadex/{slug}/{chapter_info}',
        color=discord.Color.green())
    manga_embed.set_image(
        url=f'https://uploads.mangadex.org/covers/{slug}/{cover}')
    if(author):
        manga_embed.add_field(name='Author', value=author, inline=True)
    if(artist):
        manga_embed.add_field(name='Artist', value=artist, inline=True)
    if(message_author):
        manga_embed.set_footer(text=f'Original link sent by {message_author}')
    return manga_embed


def shorten_desc(desc):
    if desc:
        if desc.count('.') < 2:
            return desc
        index_to_split = desc.find('.', desc.find('.') + 1)
        return desc[:index_to_split] + '. [...]'


client.run(TOKEN)
